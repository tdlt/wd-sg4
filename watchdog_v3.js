const socketIoClient = require('socket.io-client')
const cfg = require('./modules/cfg.js')
const request = require('request')
const checksum = require('checksum')
const exec = require('child_process').exec
const path = require('path')
const fs = require('fs')
const fse = require('fs-extra')
const cli = require('cli')
const os = require('os')
const jsonfile = require('jsonfile')
const log = require('./modules/log_helper')
const events = require('events')
const eventEmitter = new events.EventEmitter()

let config
let options
let wd_version = "4.0"
let restart_count = 0, pc_alert_count = 0
let wd = {}
let max_pc_alert_count = 5
let server_reconnect_count = 0
let player_heartbeat_time
let new_downloads = false
let status_promise = null
let screenstatus_promise = null
let downloading = false
wd.serverSocketIo = null
wd.playerSocketListener = null
wd.playerSocket = null
wd.screenSocket = null
let clientChannel = null

function init() {
    return new Promise((fulfill, reject) => {
        if (cfg.get('config') === undefined) {
            var servers = cfg.get('servers')
            var backend = {}
            var cdn = {}
            servers.forEach(function (server) {
                if (server.type === "backend") {
                    backend = server
                }
                else if (server.type === "cdn") {
                    cdn = server
                }
            })
            config = {
                "server_ip": backend.ip,
                "server_port": backend.port,
                "cdn_server_ip": cdn.ip,
                "cdn_server_port": cdn.port,
                "screen_ip": "192.168.1.246",
                "screen_port": "1515",
                "version": wd_version,
                "port": 22222,
                "player": {
                    "player_app": "player.exe",
                    "player_path": "/data/player/",
                    "log_path": "/data/playerlogs/",
                    "Process_name": "player",
                    "app_folder": "player_v0/",
                    "chk_interval": 360000
                },
                "media": {
                    "media_path": "/data/medias/"
                },
                "pid": "",
                "player_name": cfg.get('player_name'),
                "cid": "",
                "cpn_date": "",
                "downloadedAt": "",
                "log_path": "./data/logs/log.xml",
                "client_key": cfg.get('license_key'),
                "debugging": false,
                "connect_screen": false,
                "restart_player_on_failure": true,
                "restart_limit": 30,
                "server_reconnect_on_validation_failure": true,
                "server_reconnect_limit": 3,
                "player_heartbeat_interval": 5,
                "email": "support@tractive.com.my"
            }
            console.log('config 111', config);
            cfg.set('config', config)
            cfg.save()
            options = `http://${config.server_ip}:${config.server_port}`
        } else {
            config = cfg.get('config')
            config.version = wd_version
            cfg.set('config', config)
            // console.log('config 222', config)
            cfg.save()
            options = `http://${config.server_ip}:${config.server_port}`
        }
        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Starting HTTPS watchdog version ' + config.version + '\n')
        fulfill()
    })
}

init().then(() => {
    // runPlayer();
    connectToPlayer()
    setTimeout(() => {
        connectToServer()
    }, 3000)
})

function runPlayer() {
    if (os.type() != "Linux") {
        var playerPath = path.join(__dirname + config.player.player_path, config.player.app_folder)
        exec('player.exe', { cwd: playerPath }
            , function (error, stdout, stderr) {
            }
        ).on('close', function (code) {
            return code;
        });
    }
}

function connectToServer() {
    cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' WD - connecting to server');
    wd.serverSocketIo = socketIoClient(options);

    wd.serverSocketIo.on('connect', function (socket) {

        cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' WD - socket connected: ');
        console.log("wd.serverSocketIo.state", socket)
        if (wd.serverSocketIo.state == 'open') {
            if (config.pid == "") {
                registerNode()
            } else {
                connectNode()
            }

            clientChannel = wd.serverSocketIo.subscribe(config.client_key)
            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " WD - subscription on connect " + wd.serverSocketIo.subscriptions(true))

            clientChannel.watch((data) => {

                if (typeof data.players == "undefined") {

                    switch (data.action) {
                        case 'campaign:launch':
                            if (config.cid == data.campaign.cpn_cd) {
                                cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Campaign Broadcasted: " + data.campaign.cpn_cd.toUpperCase())
                                launchCampaign(data.campaign, 'launch')
                            }
                            break
                    }
                }
                else if (data.players.indexOf(config.pid) != -1) {
                    switch (data.action) {
                        case 'player:launch':
                            cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " WD - campaign launch received")
                            var campaign = data.campaign;
                            if (config.cid !== campaign.cpn_cd) {
                                cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " cpn_cd not equal. set it to config " + campaign.cpn_cd)
                                config.cid = campaign.cpn_cd
                                cfg.set('config', config)
                                cfg.save()
                            }
                            launchCampaign(campaign, 'launch')
                            break

                        case 'player:load-content':
                            var campaign = data.campaign
                            cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " WD - downloading content of campaign : " + campaign.cpn_name)
                            loadContent(campaign);
                            break

                        case 'player:restart':
                            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' WD - initiating system restart !')
                            exec('shutdown -r -f', function (err) {
                                if (err) throw err
                            })
                            break

                        case 'player:shutdown':
                            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' WD - initiating system shutdown !')
                            exec('shutdown -s -f', function (err) {
                                if (err) throw err
                            })
                            break

                        case 'player:upgrade':
                            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' WD - initiating player upgrade!')
                            upgradePlayer(data.url)
                            break

                    }
                } else {
                    console.log("WD - player id does not match")
                }
            })
        }
    });

    wd.serverSocketIo.on('player:snapshot', function (data, res) {
        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' SNAPSHOT REQUEST')
        exec('capture.exe', { cwd: __dirname }, function (error, stdout, stderr) {
            if (!error) {
                fse.readFile('MyImage.jpg', { encoding: 'base64' }, function (err, data) {
                    res(null, { img: data })
                })
            } else {
                console.log("received error")
                res(error)
            }
        });
    });
    let arr = [];
    arr.forEach(function (t) {

    })

    wd.serverSocketIo.on('player:delete:content', function (data, res) {
        let mediaFolder = path.join(__dirname, "/data/medias/")
        let uniqueMedias = {}
        let removed_files = []
        jsonfile.readFile('campaign-wd.json', function (err, obj) {
            if (err) {
                cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " campaign-wd read error: ", err)

                removeMedias(mediaFolder, [], res)

            }

            else {
                // launchCampaign(obj, 'check');
                uniqueMedias = obj.unique_medias;
                removeMedias(mediaFolder, uniqueMedias, res)

            }
        });
    });

    function removeMedias(path, campaignFiles, res) {
        let removedFiles = []

        fse.readdir(path, function (err, files) {
            if (err) {
                res(null, { success: false, data: err })

                console.log(err.toString())
            }
            else {
                files.map((file) => {
                    if (!InCampaign(file, campaignFiles)) {
                        removedFiles.push(path + file)
                    }
                })

                deleteFiles(removedFiles).then((deleted_medias) => {
                    res(null, { success: true, data: deleted_medias })
                }).catch((err) => {
                    console.log("end of loop error :", err)
                    res(err)
                })
            }
        })
    }

    function deleteFiles(files) {
        var deletes = []
        files.map((filepath) => {
            deletes.push(deleteFile(filepath))
        });

        return new Promise((resolve, reject) => {
            Promise.all(deletes)
                .then((response) => {
                    resolve(response)
                }).catch((err) => {
                    console.log("all promisses are error ", JSON.stringify(err))
                    reject(err)
                })
        })
    }

    function deleteFile(filepath) {
        return new Promise((resolve, reject) => {
            fs.stat(filepath, function (err, stats) {
                if (err)
                    console.log("stat error ", err)

                if (stats.isFile()) {
                    fs.unlink(filepath, function (err) {
                        if (err) {
                            console.log(err.toString())
                            reject(err)
                        }
                        resolve(filepath)
                    });
                }
            });
        })
    }

    function InCampaign(file, campaignFiles) {
        var match = false
        campaignFiles.map((obj) => {
            var fileArr = obj.med_name.split('.');
            var fileName = obj.med_unique_name + "." + fileArr[1]
            if (fileName == file) {
                match = true
            }
        })
        return match
    }

    wd.serverSocketIo.on('player:live', function (data, res) {
        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' PLAYER LIVE')
        if (data == config.pid) { // Request Live Status
            res(null, {
                pid: config.pid,
                live: true
            })
        }
    })

    wd.serverSocketIo.on('player:screenstatus', function (data, res) {
        cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " requesting screen status")
        let done = null
        screenstatus_promise = new Promise(function (resolve, reject) {
            done = resolve
        })
        screenstatus_promise.done = done;
        try {
            if (config.connect_screen)
                connectToScreen('status');
            else {
                wd.serverSocketIo.emit('screenstatus:result', {
                    pid: config.pid,
                    screenstatus: {
                        result: 'error',
                        message: 'Not Supported'
                    }
                })
                console.log("Set Connect Screen Config to true")
            }
        }
        catch (err) {
            wd.serverSocketIo.emit('screenstatus:result', {
                pid: config.pid,
                screenstatus: {
                    result: 'error',
                    message: err
                }
            });
            console.log("Error fetching screen status; ", err)
        }


        screenstatus_promise.then(function (screen_status) {
            console.log("got screen status ", screen_status);
            // var data = JSON.parse(screen_status);
            res(null, screen_status);
        });
        // getPlayerStatus(res)
    });

    wd.serverSocketIo.on('player:status', function (data, res) {
        console.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " requesting player status")
        let done = null
        status_promise = new Promise((resolve, reject) => {
            done = resolve
        })
        status_promise.done = done
        if (wd.playerSocket !== null) {
            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Emitting to player get statas")
            wd.playerSocket.emit('player:status', {})
        }
        else {
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " playerSocket is null")
        }

        status_promise.then((pc_status) => {
            let data = JSON.parse(pc_status)
            if (data.Param !== undefined && data.Param === "False") {
                cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " PC Status Not Sent")
                res(null, { success: false })
            }
            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " PC Status Sent")
            res(null, { success: true, data: data.Param })
        })
        // getPlayerStatus(res)
    })

    wd.serverSocketIo.on('disconnect', function (err) {
        cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " socket disconnected " + err)
        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " un-subscribing on disconnect " + wd.serverSocketIo.subscriptions(true))
        wd.serverSocketIo.destroyChannel(config.client_key)
        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " un-subscribed on disconnect " + wd.serverSocketIo.subscriptions(true))
    })

    wd.serverSocketIo.on('error', function (err) {
        cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " socket error : " + err.message)
    })

    wd.serverSocketIo.on('subscribe', function () {
        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " socket subscribed")
    })

    wd.serverSocketIo.on('unsubscribe', function (socket, channel) {
        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " socket unsubscribed")
    })
}

function getPlayerStatus(res) {
    try {
        exec("GetHardwareInfo.exe", { cwd: __dirname }, function (error, stdout, stderr) {
            if (!error) {
                let lines = stdout.split('\r\n')
                let jsonString = '{"'
                for (let i = 0; i < lines.length - 3; i++) {
                    let keyValue = lines[i].split('::')
                    let key = keyValue[0].trim()
                    let value = keyValue[1].trim()
                    jsonString += key + '":"' + value + '","'
                }
                keyValue = lines[i].split('::')
                key = keyValue[0].trim()
                value = keyValue[1].trim()
                jsonString += key + '":"' + value + '"}'
                let pcJson = JSON.parse(jsonString)
                console.log("PC status being sent to server: ", JSON.stringify(pcJson))
                res(null, { success: true, status: pcJson })
            }
        })
    }
    catch (err) {
        console.error("Error fetching pc status; ", err)
        res(null, { success: false, msg: err })
    }
}

function registerNode() {
    var playerAddress = getPlayerAddress(os.networkInterfaces());
    cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Registering WD. [NAME] " + config.player_name + " [IPv4] : " + playerAddress.ip + " [MAC] : " + playerAddress.mac)

    wd.serverSocketIo.emit('player:register', {        // send register request with: player_name.client_key,mac_address
        player_name: config.player_name,
        mac_address: playerAddress.mac,
        ip_address: playerAddress.ip,
        client_key: config.client_key
    }, function (err, res) {
        if (err) {
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Register WD Error : " + err)
        }
        if (res.valid === true) {
            cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Updating player id to ' + res.pid)
            config.pid = res.pid
            cfg.set('config', config)
            cfg.save()
            connectNode();
        } else if (res.valid === false) {
            //shutOff(res, wd);
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Register WD Validation failed : " + JSON.stringify(res))
        }
        else {
        }
    })
}

function connectNode() {
    let playerAddress = getPlayerAddress(os.networkInterfaces())
    cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Connecting WD. [IPv4] : " + playerAddress.ip + " [MAC] : " + playerAddress.mac)

    wd.serverSocketIo.emit('player:connect', {
        pid: config.pid,
        client_key: config.client_key,
        mac_address: playerAddress.mac
    }, function (err, res) {
        if (err) {
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Connect WD Error : " + err)
        }
        else if (res.valid === true) {
            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " WD Connected")
            server_reconnect_count = 0
            requestCampaign()
        }
        else if (res.valid === false) {
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' WD Connect INVALID: ' + res.valid)
            if (!config.server_reconnect_on_validation_failure) {
                //exit watchdog, leave player on
                //shutOff(res, wd);
            }
            else {
                if (server_reconnect_count < config.server_reconnect_limit) {
                    cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Validation failed! Reconnecting in 20s ...")
                    server_reconnect_count++
                    email("Server validation for player failed. Reconnect attempt: " + server_reconnect_count)
                    setTimeout(function () {
                        connectNode()
                    }, 20000)
                }
                else {
                    cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Reconnect attemp limit reached. Shutting down watchdog in 10s")
                    email("Server validation for player failed. Reconnect attempt limit reached: " + server_reconnect_count)
                    setTimeout(function () {
                        process.exit()
                    }, 10000)
                }
            }
        }
        else {
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' WD Connect NONE: ' + res.valid)
        }
    })
}

function requestCampaign() {
    wd.serverSocketIo.emit('campaign:check', {
        plr_cd: config.pid,
        client_key: config.client_key,
        cpn_cd: config.cid,
        cpn_date: config.cpn_date
    }, function (err, res) {
        if (err) {
            //runOfflineCampaign();
            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Request Campaign error " + err)
            // log.write("Request Campaign error " + err);
        }
        else {
            if (res.valid === true && res.update === true) {       // Connect valid
                let campaign = res.campaign
                console.info("config cpn_cd  :" + config.cid)
                console.info("campaign cd    :" + campaign.cpn_cd)
                console.info("config cpn_date    :" + config.cpn_date)
                console.info("campaign updatedAt :" + campaign.updatedAt)
                config.cpn_date = campaign.updatedAt
                config.cid = campaign.cpn_cd
                cfg.set('config', config)
                cfg.save();
                // if (config.cid !== campaign.cpn_cd) {
                //     cli.ok('new campaign received : ' + res.campaign.cpn_name);
                // } else if (config.cpn_date !== campaign.updatedAt) {
                //     cli.ok('campaign updated : ' + res.campaign.cpn_name);
                // } else {
                //     cli.ok('no change in campaign : ' + res.campaign.cpn_name);
                // }
                cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " 0 - " + res.msg)
                launchCampaign(campaign, 'launch')
            } else if (res.valid === true && res.update == false) {   // Connect INVALID
                //check for json file saved. if exists run launchcampaign using the content.
                cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " 1 - " + res.msg)
                checkWdCampaign()
            } else if (res.valid === false) {   // Connect INVALID
                cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " 2 - " + res.msg)
            } else {
                //runOfflineCampaign();
                cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' unknown error...')
            }
        }
    })
}

function checkWdCampaign() {
    jsonfile.readFile('campaign-wd.json', function (err, obj) {
        if (err)
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " campaign-wd read error: " + err)
        else {
            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Checking media content.")
            launchCampaign(obj, 'check')
        }
    })
}

function launchCampaign(campaign, origin) {
    // var check_flag = (typeof check == 'undefined') ? false : check;

    if (origin == 'launch') {
        saveCampaignJson('campaign-wd.json', campaign)
    }

    cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Downloading campaign content: ' + campaign.unique_medias.length)
    downloadFiles(campaign.unique_medias)
        .then(function (download_ing) {

            if (!download_ing) {
                eventEmitter.emit('download-completed')

                // emit download completed here.

                cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Download completed " + download_ing)
                config.downloadedAt = new Date();
                cfg.set('config', config)
                cfg.save()

                if (wd.playerSocket !== null) {
                    cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Emitting to player with campaign")
                    wd.playerSocket.emit('campaign_updated', {
                        campaign: campaign
                    });
                }

                if ((new_downloads && (origin == 'check')) || origin == 'launch') {
                    wd.serverSocketIo.emit('campaign:downloaded', {
                        plr_cd: config.pid,
                        client_key: config.client_key,
                        downloadedAt: config.downloadedAt
                    }, function (err) {
                        if (err) {
                            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + err);
                        } else {
                            new_downloads = false;
                            cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Download timestamp updated at check.")
                        }
                    })
                }
            }

        }).catch(function (err) {
            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Retry download in 15s seconds... ")
            setTimeout(function () {
                launchCampaign(campaign, origin)
            }, 15000)
            saveCampaignJson('campaign-wd.json', campaign)
        })
}

function loadContent(campaign) {
    // var check_flag = (typeof check == 'undefined') ? false : check;
    if (downloading) {
        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Queue loading new content.")
        eventEmitter.on('download-completed', function () {
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " this function hsouldnt be executed yet")
            loadContent(campaign)
        });
    } else {

        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Downloading campaign content: ' + campaign.unique_medias.length)
        downloadFiles(campaign.unique_medias)
            .then((download_ing) => {

                if (!download_ing) {
                    cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Download of content for " + campaign.cpn_name + " completed.")
                    // start listening to on complete from main camplain load sequence launchCampaign, when completed start loading
                }

            }).catch((err) => {
                cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Retry download in 15s seconds... ")
                setTimeout(() => {
                    loadContent(campaign)
                }, 15000)
            })
    }
}

function saveCampaignJson(fileName, obj) {

    jsonfile.writeFile(fileName, obj, function (err) {
        if (err)
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " campaign-wd save error: ", err)
    })
}

function downloadFiles(unique_medias) {
    let downloads = []

    let mediaRootPath = path.join(__dirname, config.media.media_path)

    return new Promise((fulfill, reject) => {
        if (downloading) {
            return fulfill(true)
        }
        downloading = true

        for (let i = 0; i < unique_medias.length; i++) {
            downloads.push(checkForDownload(i, unique_medias[i]))
        }

        Promise.all(downloads).then((completed_downloads) => {
            // cli.ok("Downloads finished : "+ JSON.stringify(completed_downloads));
            let unfinished_downloads = completed_downloads.filter((download) => {
                return download != null
            })
            if (unfinished_downloads.length > 0) {
                downloading = false
                return reject(unfinished_downloads)
            } else {
                downloading = false
                return fulfill(false)
            }

        }).catch((err) => {
            return reject(err)
        })

    })
}

function checkForDownload(index, unique_media) {
    let mediaRootPath = path.join(__dirname, config.media.media_path)
    let url = "https://" + config.cdn_server_ip + ":" + config.cdn_server_port + "/watchdog" + unique_media.med_url
    let fileSum = unique_media.med_hash_checksum
    let filename = url.substring(url.lastIndexOf("/") + 1).split("?")[0]
    let ext = unique_media.med_path.substring(unique_media.med_path.lastIndexOf("."))
    let filepath = mediaRootPath + filename + ext
    unique_media.path = filepath
    // return new Promise(function (fulfill, reject) {
    return new Promise((fulfill, reject) => {
        if (unique_media.med_mimetype.indexOf('application') !== -1) {
            checksum.file(filepath, function (err, sum) {
                if (sum == undefined || sum != fileSum) {
                    reqDownload(index, url, filepath, 10000, fulfill, reject, unique_media)
                } else {
                    fulfill()
                }
            });
        } else {
            fs.stat(filepath, function (err, stats) {
                if (err)
                    reqDownload(index, url, filepath, 10000, fulfill, reject, unique_media)
                else {
                    cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' [' + index + "] File Available :" + unique_media.med_cd + ' Name: ' + unique_media.med_name)
                    fulfill()
                }
            })
        }
    });

}

function reqDownload(index, url, filepath, temeout, fulfill, reject, unique_media) {

    new_downloads = true
    let options = {
        url: url,
        timeout: temeout
    }
    req = request(options);
    req.on('response', function (res) {
        cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' [' + index + "] Receiving file: " + unique_media.med_cd + ' Name: ' + unique_media.med_name)
    });
    req.on('data', function (chunk) {
    }).pipe(fse.createWriteStream(filepath + '.temp'))
        .on('error', function (err) {
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " ERROR DOWNLOAD: " + err)

            fulfill(unique_media)
        }).on('finish', function () {
        }).on('close', function (err) {
            if (err) {
                cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Download closed : ", err)
                fulfill(unique_media)
            }
            cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' [' + index + "] Download completed :" + unique_media.med_cd + ' Name: ' + unique_media.med_name)
            unique_media.path = filepath


            fs.createReadStream(filepath + '.temp').pipe(fs.createWriteStream(filepath))
                .on('error', function (err) {
                    cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Copy file error ' + err)
                    reject(err)
                })
                .on('close', function (arg) {
                    fulfill();
                    fs.unlink(filepath + '.temp', function (err) {
                        if (err) {
                            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Remove temp file ' + err.toString())
                        }
                    })
                })
        })
    req.on('error', function (err) {
        cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' [' + index + "] Error downloading media: " + unique_media.med_cd + ' Name: ' + unique_media.med_name + " " + err)

        fulfill(unique_media)
    })
}

function getPlayerAddress(interfaces) {

    let addresses = []
    for (let k in interfaces) {
        for (let k2 in interfaces[k]) {
            let address = interfaces[k][k2]
            if (address.family === 'IPv4' && !address.internal) {
                addresses.ip = address.address
                addresses.mac = address.mac
            }
        }
    }
    return addresses
}

function upgradePlayer(url) {
    cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + url)
    let filesize = 20000000
    process.update_ready = false
    let req = request({ url: url })
    let bar

    req.on('data', function (chunk) {

    })
        .pipe(fse.createWriteStream('installer.exe'))
        .on('close', function (err) {
            cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' finish downloading')
            console.log("installer.exe /SP- /verysilent key=" + config.client_key + " /noicons")

            exec('"installer.exe" /SP- /verysilent key="' + config.client_key + '" /noicons',
                function (error, stdout, stderr) {
                    process.update_ready = false

                    console.log('stdout: ' + stdout)
                    console.log('stderr: ' + stderr)
                    if (error) {
                        console.log('exec error: ' + error)
                        process.update_ready = true
                    }
                });
        })
        .on('error', function (err) {
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + err)
        })
}

function connectToPlayer() {
    wd.playerSocketListener = require('socket.io').listen(2727, { log: false })
    wd.playerSocketListener.set('heartbeat timeout', 20000)
    wd.playerSocketListener.set('heartbeat interval', 20000)
    // wd.playerSocketListener.set("log level", 0);
    wd.playerSocketListener.on('connection', function (socket) {
        cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Player Connected')
        player_heartbeat_time = (new Date()).getTime()
        wd.playerSocket = socket
        wd.playerSocket.emit('startup', {
            rootpath: __dirname + '/data/player',
            logpath: __dirname + '/data/playerlogs',
            // rootpath: 'C:\\Users\\Moshood\\Documents\\Interactive Digital Signage\\data\\player',
            // logpath: 'C:\\Users\\Moshood\\Documents\\Interactive Digital Signage\\data\\playerlogs',
            pid: config.pid
        })

        //check player status every x minutes
        let currentTime
        let playerHeartbeatTimer = setInterval(() => {
            currentTime = (new Date()).getTime()
            let timeDiff = currentTime - player_heartbeat_time
            console.log("Time Difference: ", timeDiff)
            if (timeDiff > 2 * 60000 * config.player_heartbeat_interval) {
                console.log("Player is not sending heartbeat for the interval of milliseconds: ", timeDiff)
                email("Potential player shutdown. Not sending heartbeat for the interval of milliseconds: " + timeDiff)
                console.log("PC restarting in 5s ...")
                setTimeout(() => {
                    exec('shutdown -r -f', function (err) {
                        if (err) throw err
                    })
                }, 5000)
            }
            wd.playerSocket.emit('is_live', {})
        }, 60000 * config.player_heartbeat_interval)

        // if (wd.serverSocketIo !== null)
        //     requestCampaign();

        wd.playerSocket.on('live', function (status) {
            //console.log("Received heartbeat from player: ", status);
            player_heartbeat_time = (new Date()).getTime()

            //PC Alert
            if (status == 'hdd_alert') {
                email("HDD Alert: Harddisk Usage more than 95%.")
            }
            else if (pc_alert_count < max_pc_alert_count) {
                if (status == 'mem_alert') {
                    if (pc_alert_count % 5 == 0) {
                        email("Memory Alert: Memory Usage more than 95%. Alert Count: " + pc_alert_count)
                    }
                    pc_alert_count++
                }
                else if (status == 'cpu_alert') {
                    if (pc_alert_count % 5 == 0) {
                        email("CPU Alert: CPU Usage more than 95%. Alert Count: " + pc_alert_count)
                    }
                    pc_alert_count++
                }
            }
        })

        // wd.playerSocket.on('pc_status', function (pc_status) {
        //     console.log("PC status received from player: ", pc_status);
        //     if (pc_status !== undefined && pc_status !== null) {
        //         wd.serverSocketIo.emit('pcstatus:result', {
        //             pid: config.pid,
        //             pcstatus: pc_status
        //         })
        //     }
        // });

        wd.playerSocket.on('player:status', function (pc_status) {
            status_promise.done(pc_status)
        })

        wd.playerSocket.on('playerlogs', function (playerlogs) {
            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Received logs from player')
            if (config.debugging) {
                let logs = 'Logs reported at ' + new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + '\n' +
                    '{"Player Logs" : ' + playerlogs +
                    '}'
                email(logs)
            }
        })

        wd.playerSocket.on('apperr', function (err) {
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Received error logs from player', err)
            email('Error Logs: ' + err)
        })

        wd.playerSocket.on('appcrash', function (logs) {
            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Player crashed ... \n')
            //log.write('shutting down all services ...');
            wd.playerSocket.disconnect()
            if (config.restart_player_on_failure) {
                if (restart_count < config.restart_limit) {
                    console.log("Restarting player in 5s ...")
                    setTimeout(() => {
                        runPlayer()
                        restart_count++
                        console.log("Player restarted")
                        email('Player[' + config.pid + '] crashed and restarted for ' + restart_count + ' times. Please check the logs ' + logs)
                    }, 5000)
                }
                else
                    email('Player[' + config.pid + '] crashed and has reached restart limit of ' + restart_count + ' times. Please check the logs ' + logs)
            }
            else
                email('Player[' + config.pid + '] crashed. Please check the logs ' + logs)
        });

        wd.playerSocket.on('disconnect', function () {
            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Player Socket Disconnected")
            clearInterval(playerHeartbeatTimer)
        })
    })
}

function connectToScreen(screen_command, res) {
    wd.screenSocket = new net.Socket()

    wd.screenSocket.connect(config.screen_port, config.screen_ip, function () {
        cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' CONNECTED TO Screen at: ' + config.screen_ip + ':' + config.screen_port)

        if (screen_command !== "") {
            let chunk = 'AA00010001'
            switch (screen_command) {
                case 'status':
                    chunk = 'AA00010001'
                    break
                case 'mute_on':
                    chunk = 'AA1301010116'
                    break
                case 'mute_off':
                    chunk = 'AA1301010015'
                    break
                case 'power_on':
                    chunk = 'AA1101010114'
                    break
                case 'power_off':
                    chunk = 'AA1101010013'
                    break
            }
            wd.screenSocket.write(new Buffer(chunk, 'hex'))
            console.log("Message Sent to TV")

            if (screen_command != 'status')
                wd.screenSocket.destroy()
        }
    })

    wd.screenSocket.on('error', function (error) {
        console.log("error:" + error);

        res(null, {
            pid: config.pid,
            screenstatus: {
                result: 'error',
                message: error
            }
        })


        wd.screenSocket.destroy()
    })

    wd.screenSocket.on('data', function (d) {
        let data = d.toString('hex').toUpperCase()
        console.log("screen data: ", data)
        let screenJson = {}

        switch (screen_command) {

            case 'status': {
                let input = ''
                switch (data.slice(18, data.length - 8)) {
                    case '14':
                        input = 'PC'
                    case '21':
                        input = 'HDMI'
                    case '22':
                        input = 'HDMI_PC'
                }
                let aspect = ''
                switch (data.slice(20, data.length - 6)) {
                    case '10':
                        aspect = '16:9'
                    case '18':
                        aspect = '4:3'
                    case '20':
                        aspect = 'original ratio'
                }

                let screenJson = {
                    result: 'success',
                    data: {
                        power: data.slice(12, data.length - 14),
                        volume: data.slice(14, data.length - 12),
                        mute: data.slice(16, data.length - 10),
                        input: input,
                        aspect: aspect
                    }
                };
                res(null, screenJson)
                console.log("Message Received from TV:", screenJson)
                break
            }

            //case 'mute_on' :{
            //    //console.log(data.slice(1, data.length-1))
            //    result.result.push({ mute : data.slice(12, data.length-2)})
            //    res.send(result)
            //}
            //
            //case 'mute_off' :{
            //
            //    result.result.push({ mute : data.slice(12, data.length-2)})
            //    res.send(result)
            //}
        }

        wd.screenSocket.destroy()
    })

    // Add a 'close' event handler for the client socket
    wd.screenSocket.on('close', function () {
        console.log('Connection to screen closed')
    })
}

function email(data) {
    wd.serverSocketIo.emit('player:email', {
        subject: data.subject,
        content: data.content,
        pid: config.pid
    })
}