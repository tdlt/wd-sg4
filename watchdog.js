const
    socketIoClient = require('socket.io-client'),
    cli = require('cli'),
    cfg = require('./modules/cfg.js'),
    os = require('os'),
    jsonfile = require('jsonfile'),
    path = require('path'),
    fs = require('fs'),
    fse = require('fs-extra'),
    checksum = require('checksum'),
    request = require('request'),
    exec = require('child_process').exec;

let is_debug = true;
let wd = {};
let config = {};
let options = "";
let wd_version = "4.0";
let downloading = false;
let new_downloads = false;
let restart_count = 0;
let pc_alert_count = 0;
let player_heartbeat_time;
let max_pc_alert_count = 5;

init = () => {
    return new Promise((resolve, reject) => {

        if (cfg.get('config') === undefined) {
            let servers = cfg.get('servers');
            let backend = {};
            let cdn = {};
            servers.forEach(function (server) {
                if (server.type === "backend") {
                    backend = server
                }
                else if (server.type === "cdn") {
                    cdn = server
                }
            });
            config = {
                "server_ip": backend.ip,
                "server_port": backend.port,
                "cdn_server_ip": cdn.ip,
                "cdn_server_port": cdn.port,
                "screen_ip": "192.168.1.246",
                "screen_port": "1515",
                "version": wd_version,
                "port": 22222,
                "player": {
                    "player_app": "player.exe",
                    "player_path": "/data/player/",
                    "log_path": "/data/playerlogs/",
                    "Process_name": "player",
                    "app_folder": "player_v0/",
                    "chk_interval": 360000
                },
                "media": {
                    "media_path": "/data/medias/"
                },
                "pid": "",
                "player_name": cfg.get('player_name'),
                "cid": "",
                "cpn_date": "",
                "downloadedAt": "",
                "log_path": "./data/logs/log.xml",
                "client_key": cfg.get('license_key'),
                "debugging": false,
                "connect_screen": false,
                "restart_player_on_failure": true,
                "restart_limit": 30,
                "server_reconnect_on_validation_failure": true,
                "server_reconnect_limit": 3,
                "player_heartbeat_interval": 5,
                "email": "support@tractive.com.my"
            };
            cfg.set('config', config);
            cfg.save();
            if(is_debug)
                options = `http://${config.server_ip}:${config.server_port}`;
            else
                options = `https://${config.server_ip}:${config.server_port}`;

        } else {
            config = cfg.get('config');
            config.version = wd_version;
            cfg.set('config', config);
            cfg.save();
            if(is_debug)
                options = `http://${config.server_ip}:${config.server_port}`;
            else
                options = `https://${config.server_ip}:${config.server_port}`;
        }
        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Starting HTTPS watchdog version ' + config.version + '\n');
        resolve();
    })
};

init().then(() => {
    runPlayer();
    connectToPlayer();
    setTimeout(() => {
        connectToServer()
    }, 3000)
});

function runPlayer() {
    if (os.type() !== "Linux") {
        const playerPath = path.join(__dirname + config.player.player_path, config.player.app_folder);
        exec('player.exe', {cwd: playerPath}
            , function (error, stdout, stderr) {
            }
        ).on('close', function (code) {
            return code;
        });
    }
    else{
        console.error(new Error(`OS of type ${os.type()} is not supported`));
    }
}

connectToPlayer = () => {
    wd.playerSocketListener = require('socket.io').listen(2727, {log: false});
    wd.playerSocketListener.set('heartbeat timeout', 20000);
    wd.playerSocketListener.set('heartbeat interval', 20000);
    // wd.playerSocketListener.set("log level", 0);
    wd.playerSocketListener.on('connection', function (socket) {
        cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Player Connected');
        player_heartbeat_time = (new Date()).getTime();
        wd.playerSocket = socket;
        wd.playerSocket.emit('startup', {
            rootpath: __dirname + '/data/player',
            logpath: __dirname + '/data/playerlogs',
            // rootpath: 'C:\\Users\\Moshood\\Documents\\Interactive Digital Signage\\data\\player',
            // logpath: 'C:\\Users\\Moshood\\Documents\\Interactive Digital Signage\\data\\playerlogs',
            pid: config.pid
        });

        //check player status every x minutes
        let currentTime;
        let playerHeartbeatTimer = setInterval(() => {
            currentTime = (new Date()).getTime();
            let timeDiff = currentTime - player_heartbeat_time;
            console.log("Time Difference: ", timeDiff);
            if (timeDiff > 2 * 60000 * config.player_heartbeat_interval) {
                console.log("Player is not sending heartbeat for the interval of milliseconds: ", timeDiff);
                email("Potential player shutdown. Not sending heartbeat for the interval of milliseconds: " + timeDiff);
                console.log("PC restarting in 5s ...");
                setTimeout(() => {
                    exec('shutdown -r -f', function (err) {
                        if (err) throw err
                    })
                }, 5000)
            }
            wd.playerSocket.emit('is_live', {})
        }, 60000 * config.player_heartbeat_interval);

        // if (wd.serverSocketIo !== null)
        //     requestCampaign();

        wd.playerSocket.on('live', function (status) {
            //console.log("Received heartbeat from player: ", status);
            player_heartbeat_time = (new Date()).getTime();

            //PC Alert
            if (status === 'hdd_alert') {
                email("HDD Alert: Harddisk Usage more than 95%.")
            }
            else if (pc_alert_count < max_pc_alert_count) {
                if (status === 'mem_alert') {
                    if (pc_alert_count % 5 === 0) {
                        email("Memory Alert: Memory Usage more than 95%. Alert Count: " + pc_alert_count)
                    }
                    pc_alert_count++
                }
                else if (status == 'cpu_alert') {
                    if (pc_alert_count % 5 === 0) {
                        email("CPU Alert: CPU Usage more than 95%. Alert Count: " + pc_alert_count)
                    }
                    pc_alert_count++
                }
            }
        });

        // wd.playerSocket.on('pc_status', function (pc_status) {
        //     console.log("PC status received from player: ", pc_status);
        //     if (pc_status !== undefined && pc_status !== null) {
        //         wd.serverSocketIo.emit('pcstatus:result', {
        //             pid: config.pid,
        //             pcstatus: pc_status
        //         })
        //     }
        // });

        wd.playerSocket.on('player:status', function (pc_status) {
            status_promise.done(pc_status)
        });

        wd.playerSocket.on('playerlogs', function (playerlogs) {
            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Received logs from player');
            if (config.debugging) {
                let logs = 'Logs reported at ' + new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + '\n' +
                    '{"Player Logs" : ' + playerlogs +
                    '}';
                email(logs)
            }
        });

        wd.playerSocket.on('apperr', function (err) {
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Received error logs from player', err);
            email('Error Logs: ' + err)
        });

        wd.playerSocket.on('appcrash', function (logs) {
            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Player crashed ... \n');
            //log.write('shutting down all services ...');
            wd.playerSocket.disconnect();
            if (config.restart_player_on_failure) {
                if (restart_count < config.restart_limit) {
                    console.log("Restarting player in 5s ...");
                    setTimeout(() => {
                        runPlayer();
                        restart_count++;
                        console.log("Player restarted");
                        email('Player[' + config.pid + '] crashed and restarted for ' + restart_count + ' times. Please check the logs ' + logs)
                    }, 5000)
                }
                else
                    email('Player[' + config.pid + '] crashed and has reached restart limit of ' + restart_count + ' times. Please check the logs ' + logs)
            }
            else
                email('Player[' + config.pid + '] crashed. Please check the logs ' + logs)
        });

        wd.playerSocket.on('disconnect', function () {
            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Player Socket Disconnected");
            clearInterval(playerHeartbeatTimer)
        })
    })
};

connectToServer = () => {
    console.log("options", options);
    cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' WD - connecting to server');
    wd.serverSocket = socketIoClient(options);
    wd.serverSocket.on('connect', (socket) => {
        cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' WD - socket connected: ');
        if (wd.serverSocket.connected) {
            if (config.pid === "") {
                registerNode()
            } else {
                connectNode();
            }
        }

        wd.serverSocket.on(config.client_key, (data) => {
            console.log(config.client_key, data);
            let campaign;
            if (data.players && data.players.length > 0 && data.players.indexOf(config.pid) > -1) {
                switch (data.action) {
                    case 'player:launch':
                        cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " WD - campaign launch received");
                        campaign = data.campaign;
                        if (config.cid !== campaign._id) {
                            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " cpn_cd not equal. set it to config " + campaign._id);
                            config.cid = campaign._id;
                            cfg.set('config', config);
                            cfg.save()
                        }
                        launchCampaign(campaign, 'launch');
                        break;

                    case 'player:load-content':
                        campaign = data.campaign;
                        cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " WD - downloading content of campaign : " + campaign.name);
                        loadContent(campaign);
                        break;

                    case 'player:restart':
                        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' WD - initiating system restart !');
                        exec('shutdown -r -f', function (err) {
                            if (err) throw err
                        });
                        break;
                    case 'player:shutdown':
                        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' WD - initiating system shutdown !');
                        exec('shutdown -s -f', function (err) {
                            if (err) throw err
                        });
                        break;

                    case 'player:upgrade':
                        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' WD - initiating player upgrade!');
                        upgradePlayer(data.url);
                        break
                }
            }
        })
    });

    wd.serverSocket.on('player::restart', function (data, res) {
        console.log('player::restart', data);
        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' WD - initiating system restart !');
        // exec('shutdown -r -f', function (err) {
        //     if (err) throw err
        // });
        res(null, {
            success: true
        })
    });

    wd.serverSocket.on('player::live', function (data, res) {
        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' PLAYER LIVE');
        if (data === config.pid) { // Request Live Status
            res(null, {
                pid: config.pid,
                live: true
            })
        }
    });

    wd.serverSocket.on('player::snapshot', function (data, res) {
        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' SNAPSHOT REQUEST');
        exec('capture.exe', {cwd: __dirname}, function (error, stdout, stderr) {
            if (!error) {
                fse.readFile('MyImage.jpg', {encoding: 'base64'}, function (err, data) {
                    res(null, {img: data})
                })
            } else {
                console.log("received error");
                res(error)
            }
        });
    });

    wd.serverSocket.on('disconnect', function (err) {
        cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " socket disconnected " + err)
    });

    wd.serverSocket.on('error', function (err) {
        cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " socket error : " + err.message)
    });

    wd.serverSocket.on('subscribe', function () {
        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " socket subscribed")
    });

    wd.serverSocket.on('unsubscribe', function (socket, channel) {
        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " socket unsubscribed")
    })

};

registerNode = () => {
    let playerAddress = getPlayerAddress(os.networkInterfaces());
    cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Registering WD. [NAME] " + config.player_name + " [IPv4] : " + playerAddress.ip + " [MAC] : " + playerAddress.mac);

    wd.serverSocket.emit('player:register', {        // send register request with: player_name.client_key,mac_address
        player_name: config.player_name,
        mac_address: playerAddress.mac,
        ip_address: playerAddress.ip,
        client_key: config.client_key
    }, (res) => {
        if (res.success) {
            cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Updating player id to ' + res.player._id);
            config.pid = res.player._id;
            cfg.set('config', config);
            cfg.save();
            connectNode();
        } else {
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Register WD Validation failed : " + res.error)
        }
    })
};

connectNode = () => {
    let playerAddress = getPlayerAddress(os.networkInterfaces());
    cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Connecting WD. [IPv4] : " + playerAddress.ip + " [MAC] : " + playerAddress.mac);

    wd.serverSocket.emit('player:connect', {
        player: config.pid,
        client_key: config.client_key,
        mac_address: playerAddress.mac
    }, (res) => {
        if (res.success) {
            requestCampaign();
        } else {
            // todo
        }
    })

};

requestCampaign = () => {
    wd.serverSocket.emit('campaign:check', {
        player: config.pid,
        client_key: config.client_key,
        campaign: config.cid,
        campaign_date: config.cpn_date
    }, (res) => {
        if (res.success === true && res.update === true) {
            let campaign = res.campaign;
            config.cpn_date = campaign.updatedAt;
            config.cid = campaign._id;
            cfg.set('config', config);
            cfg.save();
            launchCampaign(campaign, 'launch')
        }
    })
};

launchCampaign = (campaign, origin) => {
    if (origin === 'launch') {
        saveCampaignJson('campaign-wd.json', campaign)
    }

    cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Downloading campaign content: ' + campaign.unique_medias.length);
    downloadFiles(campaign.unique_medias).then(() => {

    }).catch(function (err) {
        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Retry download in 15s seconds... ");
        setTimeout(function () {
            launchCampaign(campaign, origin)
        }, 15000)
    })
};

saveCampaignJson = (fileName, obj) => {
    jsonfile.writeFile(fileName, obj, function (err) {
        if (err)
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " campaign-wd save error: ", err)
    })
};

downloadFiles = (unique_medias) => {
    let downloads = [];

    let mediaRootPath = path.join(__dirname, config.media.media_path);

    return new Promise((fulfill, reject) => {
        if (downloading) {
            return fulfill(true)
        }
        downloading = true;

        for (let i = 0; i < unique_medias.length; i++) {
            downloads.push(checkForDownload(i, unique_medias[i]))
        }

        Promise.all(downloads).then((completed_downloads) => {
            // cli.ok("Downloads finished : "+ JSON.stringify(completed_downloads));
            let unfinished_downloads = completed_downloads.filter((download) => {
                return download !== null
            })
            if (unfinished_downloads.length > 0) {
                downloading = false;
                return reject(unfinished_downloads)
            } else {
                downloading = false;
                return fulfill(false)
            }

        }).catch((err) => {
            return reject(err)
        })

    })
};

checkForDownload = (index, unique_media) => {
    let mediaRootPath = path.join(__dirname, config.media.media_path);
    let url = "";
    if(is_debug)
        url = "http://" + config.cdn_server_ip + ":" + config.cdn_server_port + "/cdn/watchdog" + unique_media.url;
    else
        url = "https://" + config.cdn_server_ip + ":" + config.cdn_server_port + "/cdn/watchdog" + unique_media.url;
    let fileSum = unique_media.checksum;
    let filename = url.substring(url.lastIndexOf("/") + 1).split("?")[0];
    let ext = unique_media.path.substring(unique_media.path.lastIndexOf("."));
    let filepath = mediaRootPath + filename + ext;
    unique_media.path = filepath;
    // return new Promise(function (fulfill, reject) {
    return new Promise((fulfill, reject) => {
        if (unique_media.mimetype.indexOf('application') !== -1) {
            checksum.file(filepath, function (err, sum) {
                if (sum === undefined || sum !== fileSum) {
                    reqDownload(index, url, filepath, 10000, fulfill, reject, unique_media)
                } else {
                    fulfill()
                }
            });
        } else {
            fs.stat(filepath, function (err, stats) {
                if (err)
                    reqDownload(index, url, filepath, 10000, fulfill, reject, unique_media)
                else {
                    cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' [' + index + "] File Available :" + unique_media._id + ' Name: ' + unique_media.name);
                    fulfill()
                }
            })
        }
    });

};

reqDownload = (index, url, filepath, temeout, fulfill, reject, unique_media) => {

    new_downloads = true;
    let options = {
        url: url,
        timeout: temeout
    };
    req = request(options);
    req.on('response', function (res) {
        cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' [' + index + "] Receiving file: " + unique_media._id + ' Name: ' + unique_media.name)
    });
    req.on('data', function (chunk) {
    }).pipe(fse.createWriteStream(filepath + '.temp'))
        .on('error', function (err) {
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " ERROR DOWNLOAD: " + err);

            fulfill(unique_media)
        }).on('finish', function () {
    }).on('close', function (err) {
        if (err) {
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Download closed : ", err);
            fulfill(unique_media)
        }
        cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' [' + index + "] Download completed :" + unique_media._id + ' Name: ' + unique_media.name);
        unique_media.path = filepath;


        fs.createReadStream(filepath + '.temp').pipe(fs.createWriteStream(filepath))
            .on('error', function (err) {
                cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Copy file error ' + err);
                reject(err)
            })
            .on('close', function (arg) {
                fulfill();
                fs.unlink(filepath + '.temp', function (err) {
                    if (err) {
                        cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Remove temp file ' + err.toString())
                    }
                })
            })
    });
    req.on('error', function (err) {
        cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' [' + index + "] Error downloading media: " + unique_media._id + ' Name: ' + unique_media.name + " " + err);

        fulfill(unique_media)
    })
};

upgradePlayer = (url) => {
    cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + url);
    let filesize = 20000000;
    process.update_ready = false;
    let req = request({url: url});
    let bar;

    req.on('data', function (chunk) {

    })
        .pipe(fse.createWriteStream('installer.exe'))
        .on('close', function (err) {
            cli.ok(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' finish downloading');
            console.log("installer.exe /SP- /verysilent key=" + config.client_key + " /noicons");

            exec('"installer.exe" /SP- /verysilent key="' + config.client_key + '" /noicons',
                function (error, stdout, stderr) {
                    process.update_ready = false;

                    console.log('stdout: ' + stdout);
                    console.log('stderr: ' + stderr);
                    if (error) {
                        console.log('exec error: ' + error)
                        process.update_ready = true
                    }
                });
        })
        .on('error', function (err) {
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + err)
        })
};

function loadContent(campaign) {
    // var check_flag = (typeof check == 'undefined') ? false : check;
    if (downloading) {
        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Queue loading new content.");
        eventEmitter.on('download-completed', function () {
            cli.error(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " this function hsouldnt be executed yet");
            loadContent(campaign)
        });
    } else {

        cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + ' Downloading campaign content: ' + campaign.unique_medias.length);
        downloadFiles(campaign.unique_medias)
            .then((download_ing) => {

                if (!download_ing) {
                    cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Download of content for " + campaign.name + " completed.")
                    // start listening to on complete from main camplain load sequence launchCampaign, when completed start loading
                }

            }).catch((err) => {
            cli.info(new Date().toISOString().substring(0, 10) + " " + new Date().toLocaleTimeString() + " Retry download in 15s seconds... ");
            setTimeout(() => {
                loadContent(campaign)
            }, 15000)
        })
    }
}

getPlayerAddress = (interfaces) => {
    let addresses = [];
    for (let k in interfaces) {
        for (let k2 in interfaces[k]) {
            let address = interfaces[k][k2];
            if (address.family === 'IPv4' && !address.internal) {
                addresses.ip = address.address;
                addresses.mac = address.mac;
            }
        }
    }
    return addresses
};

function email(data) {
    wd.serverSocket.emit('player:email', {
        subject: data.subject,
        content: data.content,
        pid: config.pid
    })
}